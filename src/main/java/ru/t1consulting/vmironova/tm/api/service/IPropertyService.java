package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    Integer getServerPort();

}
