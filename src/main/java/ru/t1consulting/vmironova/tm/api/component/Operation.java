package ru.t1consulting.vmironova.tm.api.component;

import ru.t1consulting.vmironova.tm.dto.request.AbstractRequest;
import ru.t1consulting.vmironova.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
