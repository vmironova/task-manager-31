package ru.t1consulting.vmironova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;
import ru.t1consulting.vmironova.tm.dto.Domain;
import ru.t1consulting.vmironova.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = System.getProperty("java.io.tmpdir") + "data.base64";

    @NotNull
    public static final String FILE_BACKUP = System.getProperty("java.io.tmpdir") + "backup.base64";

    @NotNull
    public static final String FILE_BINARY = System.getProperty("java.io.tmpdir") + "data.bin";

    @NotNull
    public static final String FILE_XML = System.getProperty("java.io.tmpdir") + "data.xml";

    @NotNull
    public static final String FILE_JSON = System.getProperty("java.io.tmpdir") + "data.json";

    @NotNull
    public static final String FILE_YAML = System.getProperty("java.io.tmpdir") + "data.yaml";

    @NotNull
    public final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public final String APPLICATION_TYPE_JSON = "application/json";

    public AbstractDataCommand() {
    }

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        assert serviceLocator != null;
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        assert serviceLocator != null;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
